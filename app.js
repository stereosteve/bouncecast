angular.module('bouncecast', ['firebase', 'ngRoute', 'ngSanitize'])
.config(function($routeProvider) {
  $routeProvider

  .when('/', {
    templateUrl: 'pages/home.html',
    controller: 'homeCtrl',
  })

  .when('/rooms/:id', {
    templateUrl: 'pages/room.html',
    controller: 'roomCtrl',
  })

})




.controller('homeCtrl', function($scope, $firebase, $location) {
  var roomsRef = new Firebase("https://bouncecast.firebaseio.com/rooms");
  $scope.rooms = $firebase(roomsRef);

  $scope.addRoom = function() {
    // AngularFire $add method
    var newRef = roomsRef.push();
    $scope.newRoom.id = newRef.name();
    newRef.set($scope.newRoom);
    $scope.newRoom = undefined;
  }

  $scope.showRoom = function(room) {
    $location.path('/rooms/' + room.id)
  }
})




.controller('roomCtrl', function($scope, $routeParams, $firebase) {
  var roomRef = new Firebase("https://bouncecast.firebaseio.com/rooms/" + $routeParams.id);
  $scope.room = $firebase(roomRef);
  $scope.save = function() {
    $scope.room.$save();
    $scope.editMode = false;
  }

  $scope.room.$on('value', function(val) {
    console.log('new val', val)
  })
})






.filter('formatMsg', function() {
  return function(msg) {
    if (!msg) return
    return msg.replace(/\n/g, '<br>')
  }
})